import express from 'express';
import graphqlHTTP from 'express-graphql';
import { GraphQLSchema } from 'graphql';

import { ContestQuery, ContestMutation } from './resources/contest';

// Start MONGO
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const db = mongoose.connect('mongodb://mongo/coffer', { useMongoClient: true });

db.on('error', (err) => {
    console.error('Error on Mongo connect', err);
});
db.once('open', () => {
    var app = express();

    // Create route
    app.use('/graphiql', graphqlHTTP({
        schema: new GraphQLSchema({
            query: ContestQuery,
            mutation: ContestMutation
        }),
        graphiql: true,
    }));

    app.use('/contests', graphqlHTTP({
        schema: new GraphQLSchema({
            query: ContestQuery,
            mutation: ContestMutation
        }),
    }));

    // Start server
    app.listen(4000, () => console.info('Now browse to localhost:4000/graphiql'));
});
