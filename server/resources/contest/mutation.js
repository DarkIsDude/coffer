import { GraphQLNonNull, GraphQLString, GraphQLObjectType } from 'graphql';
import { GraphQLDate } from 'graphql-iso-date';
import ContestType from './type';
import Contest from './model';

const MutationAdd = {
    type: ContestType,
    description: 'Add a contest',
    args: {
        theme: { name: 'Contest title', type: new GraphQLNonNull(GraphQLString) },
        start: { name: 'Contest title', type: new GraphQLNonNull(GraphQLDate) },
        end: { name: 'Contest title', type: new GraphQLNonNull(GraphQLDate) },
    },
    resolve: (root, args) => {
        return new Contest({
            theme: args.theme,
            start: args.start,
            end: args.end,
            completed: false
        }).save();
    }
};

const MutationUpdate = {
    type: ContestType,
    description: 'Update a contest',
    args: {
        _id: { name: 'Contest id', type: new GraphQLNonNull(GraphQLString) },
        theme: { theme: 'Contest title', type: GraphQLString },
    },
    resolve: async (root, args) => {
        const contest = await Contest.findOne({ _id: args._id });

        if (args.theme)
            contest.theme = args.theme;

        return await contest.save();
    }
};

export default new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        add: MutationAdd,
        update: MutationUpdate,
    }
});
