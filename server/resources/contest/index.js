import { Contest as model } from './model';
import { ContestType as type } from './type';
import Query from './query';
import Mutation from './mutation';

export const Contest = model;
export const ContestType = type;
export const ContestQuery = Query;
export const ContestMutation = Mutation;