import { GraphQLID, GraphQLObjectType, GraphQLString, GraphQLBoolean } from 'graphql';
import { GraphQLDate } from 'graphql-iso-date';

export default new GraphQLObjectType({
    name: 'Contest',
    fields: () => {
        return {
            _id: { type: GraphQLID },

            theme: { type: GraphQLString },
            completed: { type: GraphQLBoolean },

            start: { type: GraphQLDate },
            end: { type: GraphQLDate },
        }
    }
});