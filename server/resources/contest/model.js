import mongoose from 'mongoose';

export default mongoose.model('contest', {
    theme: String,
    completed: Boolean,

    start: Date,
    end: Date,
});
