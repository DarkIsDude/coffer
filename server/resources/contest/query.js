import { GraphQLObjectType, GraphQLList, GraphQLInt, GraphQLString } from 'graphql';
import ContestType from './type';
import Contest from './model';

export default new GraphQLObjectType({
    name: 'Query',
    fields: () => {
        return {
            contests: {
                type: new GraphQLList(ContestType),
                args: {
                    id: { type: GraphQLInt },
                    theme: { type: GraphQLString },
                },
                resolve: (root, args) => Contest.find(args),
            }
        }
    }
});