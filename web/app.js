const app      = require('express')();
const config   = require('config');
const fs       = require('fs');
const renderer = require('vue-server-renderer');
const Vue      = require('vue');

const port = config.get('web.port');

app.get('/', (request, response) => {
    const contests = new Vue({
        data: {
            contests: [
                { start: '2017-05-12', end: '2017-05-27', theme: 'Drink, not drunk' },
                { start: '2017-06-01', end: '2017-06-16', theme: 'Jump!' },
                { start: '2017-06-19', end: '2017-06-25', theme: '42 is the answer' }
            ]
        },
        template: `
            <ul>
                <li v-for="contest in contests">{{ contest.theme }} <i>({{ contest.start}} - {{ contest.end }})</i></li>
            </ul>
        `
    });

    const index = renderer.createRenderer({
        template: fs.readFileSync('web/public/index.html', 'utf8')
    });

    index.renderToString(contests, (error, html) => {
        if (error) {
            response.status(500).end('Internal server error');
            return;
        }

        response.end(html);
    });
});

app.listen(port, () => console.log('Listening on port', port));
