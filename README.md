# COFFER

A simple photography contest manager. (name generated with <https://mrsharpoblunto.github.io/foswig.js/>)

## technical choices

- Node
- Express
- GraphQL
- View

## Docker

You can launch the project with `docker-compose up`. This will create a new DB, all data stored in mongo_data.

## Contribute

### Create a release

Upgrade version in

- server/package.json
- web/package.json
- server/Dockerfile
- web/Dockerfile
- docker-compose.yml